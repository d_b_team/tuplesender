package entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.bean.CsvBindByName;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Tuple {

    @CsvBindByName
    String approveDate;

    @CsvBindByName
    String articleID;

    @CsvBindByName
    String articleWordCount;

    @CsvBindByName
    String commentID;

    @CsvBindByName
    String commentType;

    @CsvBindByName
    String createDate;

    @CsvBindByName
    String depth;

    @CsvBindByName
    String editorsSelection;

    @CsvBindByName
    String inReplyTo;

    @CsvBindByName
    String parentUserDisplayName;

    @CsvBindByName
    String recommendations;

    @CsvBindByName
    String sectionName;

    @CsvBindByName
    String userDisplayName;

    @CsvBindByName
    String userID;

    @CsvBindByName
    String userLocation;

    public Tuple(String articleID, String createDate) {
        this.articleID = articleID;
        this.createDate = createDate;
        this.approveDate = "11111111111111111111";
        this.articleWordCount = "2222";
        this.commentID = "11111112";
        this.commentType = "inReplyTo";
        this.depth = "2";
        this.editorsSelection = "true";
        this.inReplyTo = "12345678";
        this.parentUserDisplayName = "ciao";
        this.recommendations = "3";
        this.sectionName = "name";
        this.userDisplayName = "ciaoname";
        this.userID = "1263849";
        this.userLocation = "loc";
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }
}
